#include "double_linked_list.h"

int main() {
  printf("========================================\n");
  printf("TEST Get Search (Index, Element) Delete \n");
  printf("========================================\n");
  struct node test_list = init(0);
  struct node* test_list_addr = &test_list;
  // printf("HEAD VALUE %d\n",test_list_addr->data );
  test_list_addr = push_front(test_list_addr, 20);
  test_list_addr = push_front(test_list_addr, 30);
  test_list_addr = push_front(test_list_addr, 2);
  test_list_addr = push_front(test_list_addr, 40);
  test_list_addr = push_front(test_list_addr, 56); //head

  printf("\nNode List:  ");
  print_list(test_list_addr);
  printf("Length: %d", length(test_list_addr));
  printf("\n");

  int get_index = 4;
  struct node* get_node = get(test_list_addr, get_index);
  printf("Value of index %d: %d\n", get_index, get_node->data );

  int to_find = 30;
  printf("Index of %d: %d\n", to_find, find_index_of(test_list_addr, to_find));

  int index_to_delete = 3;
  struct node* deleted_node = delete_index(test_list_addr, index_to_delete);
  printf("Value of index deleted at %d: %d\n", index_to_delete, deleted_node->data);
  printf("After Deletion: \t");
  print_list(test_list_addr);

  destroy(test_list_addr);
  printf("\n========================================\n\n");


  printf("TEST APPEND\n");
  printf("========================================\n");

  struct node test_head = init(1);
  struct node* test_head_addr = &test_head;
  test_head_addr = push_front(test_head_addr, 123);
  push_last(test_head_addr, 456);
  printf("1st list as:\n\t");
  print_list(test_head_addr);

  struct node test_back = init(2);
  struct node* test_back_addr = &test_back;
  push_last(test_back_addr, 123);
  test_back_addr = push_front(test_back_addr, 789);
  printf("\n\n2nd list as:\n\t");
  print_list(test_back_addr);

  append(test_head_addr, test_back_addr);
  printf("\n\nAfter append: \n\t");
  print_list(test_head_addr);
  printf("\n========================================\n");

  return 0;
}
