#include "double_linked_list.h"

struct node init(int data){
  struct node head = {data, NULL, NULL};

  return head;
}

// Null values for everynode in the list and frees its pointers
void destroy(struct node* head_a){
  struct node* tmp;

   while (head_a != NULL)
    {
       tmp = head_a;
       head_a = head_a->next;
       tmp = NULL;
       free(tmp);

       if(head_a == NULL){
         break;
       }
    }

    head_a = NULL;
    free(head_a);
}

int length(struct node* head_a) {
   int length = 0;
   struct node *current = head_a;
   while(current != NULL){
      current = current->next;
      length++;
   }
   current = NULL;
   free(current);
   return length;
}

//display the list in from first to last
void print_list(struct node* head_a) {
   //start from the beginning
   struct node *ptr = head_a;

   while(ptr != NULL) {
      printf("(%d)-> ", ptr->data);
      ptr = ptr->next;
   }

   printf("NULL\n");
   ptr = NULL;
   free(ptr);
}


//insert at head
struct node* push_front(struct node* head_a, int data) {

   //create temp node
   struct node* head_temp = (struct node*) malloc(sizeof(struct node));
   head_temp->data = data;

   if(head_a == NULL) {
      //return newly created node
      head_a = head_temp;
      return head_a;
   } else {
      //update front prev
      head_a->prev = head_temp;
   }

   //point it to old first link
   head_temp->next = head_a;
   head_temp->prev = NULL;

   //point first to new first link
   head_a = head_temp;
   return head_a;
}

//insert link at the last location
void push_last(struct node* head_a, int data) {

   //create a link
   struct node* tail_temp = (struct node*) malloc(sizeof(struct node));
   tail_temp->data = data;

   if(head_a == NULL) {
      //make it the last node
      head_a = tail_temp;
   } else {

      struct node* current = head_a;

      while(current->next != NULL){
        current = current->next;
      }

      current->next = tail_temp;
      //last node as prev of new node
      tail_temp->prev = current;
      tail_temp->prev = NULL;
   }
}

// append 2 nodes front->last->next = back; back->prev = front->last
void append(struct node* front, struct node* back){
  if(front == NULL ){
    front = back;
  } else if(back == NULL){
    return;
  } else{
    struct node* current = front;

    while(current->next != NULL){
      current = current->next;
    }

    current->next = back;
    //last node as prev of new node
    back->prev = current;
  }
}

//delete a link at given index
struct node* delete_index(struct node* head_a, int index) {

  //start from the first link
  struct node* current = head_a;

  int temp_index = index;

  //if list is empty
  if(head_a == NULL) {
    return NULL;
  }

  while(temp_index > 0) {
   if(current->next == NULL) {
      return NULL;
   } else {
      //move to next node
      current = current->next;
   }
   temp_index--;
  }

  //if at the front
  if(current == head_a) {
    //change head next to new node
    head_a = head_a->next;
  } else {
    //bypass the current link
    current->prev->next = current->next;
  }

  return current;
}

//get node at given index
struct node* get(struct node* head_a, int index){
  //start from head
  int temp_index = index;
  struct node* current = head_a;

  if(head_a == NULL) {
     return NULL;
  }

  while(temp_index > 0) {
     if(current->next == NULL) {
        return NULL;
     } else {
        current = current->next;
     }
     temp_index--;
  }

  return current;
}

// find index of given value
int find_index_of(struct node* head_a, int val){
  //start from the first link
  struct node *current = head_a;

  //if list is empty
  if(head_a == NULL) {
     return -1;
  }
  int n=0;
  //navigate through list
  while(current->data != val) {
     //if it is last node and can't find value
     if(current->next == NULL) {
        return -1;
     } else {
        current = current->next;
        n++;
     }
  }
  return n;
}
