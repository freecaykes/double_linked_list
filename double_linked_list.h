#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#ifndef DOUBLE_LINKED_LIST_H_
#define DOUBLE_LINKED_LIST_H_

struct node {
   int data;

   struct node *next;
   struct node *prev;
};

extern struct node init(int data);
extern void destroy(struct node* head_a);
extern int length(struct node* head_a);

extern void print_list(struct node* head_a);

extern struct node* push_front(struct node* head_a, int data);
extern void push_last(struct node* head_a, int data);
extern void append(struct node* front, struct node* back);
extern struct node* delete_index(struct node* head_a, int index);

extern int find_index_of(struct node* head_a, int val);
extern struct node* get(struct node* head_a, int index);

#endif // DOUBLE_LINKED_LIST_H_
