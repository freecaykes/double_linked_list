main: main.o double_linked_list.o
	gcc main.o double_linked_list.o -o main

main.o: main.c double_linked_list.h
	gcc -c main.c

double_linked_list.o: double_linked_list.c double_linked_list.h
	gcc -g -Wall -c double_linked_list.c

clean:
	rm -f main *.o
